using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool parsed = false;
            int upperLimit = 0;
            while (!parsed) {
                Console.Write("Введите число: ");
                string str = Console.ReadLine();
                parsed = Int32.TryParse(str, out upperLimit);
            }

            for (int i = 1; i <= upperLimit; i++)
            {
                Console.Write(i);
                Console.Write(' ');
            }

            Console.WriteLine();
        }
    }
}